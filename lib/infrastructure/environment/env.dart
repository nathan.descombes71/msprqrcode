import 'package:get/get.dart';

import 'env_type.dart';

class Env extends GetxService {
  // ┍━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┑
  // ┃ Environment                                                     ┃
  // ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┙

  EnvType kEnv = EnvType.dev();

  String kJWT =
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcHAueGVmaWFwcHMubG9jYWwiLCJhdWQiOiJleUpoYkdjaU9pSnphR0V5TlRZaUxDSjBlWEFpT2lKQlMxUWlmUS5leUpyWlhraU9pSXpTWGt6T1dkalZXYzJkelpUTm5OVkluMC4yMzVhOTNmNjI2MGEwOGMwMDg5NTRiMjg1MDM3ZjU1YmY0ZDVlNWZmYjhlNDgzYTE2YTIwOTU0MjhlN2M0MTRmIiwianRpIjoieWp4dG81RWQzdDRoU2tyZiIsImlhdCI6MTYxNDA3MDg1MSwibmJmIjoxNjE0MDcwODUxLCJleHAiOjE2MTQwNzQ0NTEsImtleVJpbmciOiJleUpRYjNKMFlXbHNJam9pWlhsS2FHSkhZMmxQYVVwNllVZEZlVTVVV1dsTVEwb3daVmhCYVU5cFNrSlRNVkZwWmxFdVpYbEtjbHBZYTJsUGFVcHNaRmhSZDAxSFNrOVZlbWh4WWxWc1JVNVVWazlKYmpBdU9UWTBaR1prTURBd00yVXhOR1EyTm1Gak5tVmhNbVl4T1dJeFlqRmxZV1ZtT1RFME4yVTNPRFV4TWpjek1HSTVNakJtTkRJME1qTmpPRGs1TkRsbFpDSXNJbFpsYm1SbGRYSWlPaUpVVHlCVlVFUkJWRVVnSVNFaElURWlMQ0pGYldGeVoyVnRaVzUwSWpvaVZFOGdWVkJFUVZSRklDRWhJU0V5SWl3aVVYVmxjM1JwYjI1dVlXbHlaU0k2SWxSUElGVlFSRUZVUlNBaElTRWhNeUlzSWs1dmRHVWdaR1VnWm5KaGFYTWlPaUpVVHlCVlVFUkJWRVVnSVNFaElUUWlMQ0pEWVhKMFpTQmtaU0IyYVhOcGRHVWlPaUpVVHlCVlVFUkJWRVVnSVNFaElUVWlMQ0pKYm1kbGJtbGxjbWxsSWpvaVZFOGdWVkJFUVZSRklDRWhJU0UySWl3aVEyOXVaMlZ6SWpvaVpYbEthR0pIWTJsUGFVcDZZVWRGZVU1VVdXbE1RMG93WlZoQmFVOXBTa0pUTVZGcFpsRXVaWGxLY2xwWWEybFBhVWw2VTFocmVrOVhaR3BXVjJNeVpIcGFWRTV1VGxaSmJqQXVNak0xWVRrelpqWXlOakJoTURoak1EQTRPVFUwWWpJNE5UQXpOMlkxTldKbU5HUTFaVFZtWm1JNFpUUTRNMkV4Tm1FeU1EazFOREk0WlRkak5ERTBaaUo5IiwidXVpZCI6IjZlM2Q4NGI1LThkYzMtNWFiMC1iNmU5LTFiN2Y1ZDlhMzAyOCIsImNsaWVudF9pZCI6IjEiLCJjbGllbnRfdXVpZCI6IjQyZGQ1ODBlLTBlMGQtNTc2Mi1hM2EyLTgzYmZmNzFmYzQ3MiJ9.81KTtOlvbf5h8t11tMbmdaUy9u1Bw4W_ElUlEt6yVEI';

  String kDefaultUsername = 'utilisateur.client3@xefiapps.fr';
  String kDefaultPassword = 'I am root';

  bool get showDebugBanner => kEnv != EnvType.prod();

  // ┍━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┑
  // ┃ AppKeys                                                         ┃
  // ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┙

  String get kAppKey {
    return kEnv.when(
      dev: () =>
          'eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJBS1QifQ.eyJrZXkiOiIzSXkzOWdjVWc2dzZTNnNVIn0.235a93f6260a08c008954b285037f55bf4d5e5ffb8e483a16a2095428e7c414f',
      rd: () =>
          'eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJBS1QifQ.eyJrZXkiOiIzSXkzOWdjVWc2dzZTNnNVIn0.235a93f6260a08c008954b285037f55bf4d5e5ffb8e483a16a2095428e7c414f',
      rc: () =>
          'eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJBS1QifQ.eyJrZXkiOiIzSXkzOWdjVWc2dzZTNnNVIn0.235a93f6260a08c008954b285037f55bf4d5e5ffb8e483a16a2095428e7c414f',
      prod: () =>
          'eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJBS1QifQ.eyJrZXkiOiIzSXkzOWdjVWc2dzZTNnNVIn0.235a93f6260a08c008954b285037f55bf4d5e5ffb8e483a16a2095428e7c414f',
    );
  }

  // ┍━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┑
  // ┃ Base Urls                                                       ┃
  // ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┙

  String get kBaseUrl {
    return kEnv.when(
      dev: () => 'http://172.18.0.8/api/v1',
      rc: () => 'http://172.18.0.14:80',
      rd: () => 'http://172.18.0.14:80',
      prod: () => 'http://172.18.0.14:80',
    );
  }

 
}
