
import 'package:get/get.dart';
import 'package:msprqrcode/presentation/views/main_page/main_page_view.dart';
import 'package:msprqrcode/presentation/views/main_page/main_page_view_controller_bindings.dart';
import 'package:msprqrcode/presentation/views/qr_code_scan/qr_code_scan_view.dart';
import 'package:msprqrcode/presentation/views/qr_code_scan/qr_code_scan_view_controller_bindings.dart';

import 'routes.dart';

class Nav {
  static List<GetPage> routes = [
    /// REVIEW [Mobile & Web] routes
    GetPage(
      name: Routes.MAINPAGE,
      page: () => MainPageView(),
      binding: MainPageViewControllerBindings(),
    ),
   GetPage(
      name: Routes.SCANQRCODE,
      page: () => QrCodeScanView(),
      binding: QrCodeScanViewControllerBindings(),
    ),
  ];
}
