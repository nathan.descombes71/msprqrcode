import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'qr_code_scan_view_controller.dart';

class QrCodeScanView extends GetView<QrCodeScanViewController> {
  QrCodeScanView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Janus",
          style: TextStyle(fontFamily: 'Raleway'),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Get.theme.primaryColor,
              height: 400,
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                    width: 200.0,
                    height: 200.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
