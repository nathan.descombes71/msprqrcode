import 'package:get/get.dart';

import '../views_mobile.exports.dart';
import 'qr_code_scan_view_controller.dart';

class QrCodeScanViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => QrCodeScanViewController(),
    );
  }
}
