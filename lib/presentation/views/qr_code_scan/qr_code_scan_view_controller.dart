import 'package:flutter/material.dart';
import 'package:get/get.dart';


class QrCodeScanViewController extends GetxController with StateMixin {
  QrCodeScanViewController();

  PageController pageController = PageController();

  final RxInt currentIndex = 0.obs;

  @override
  void onInit() {
    change(null, status: RxStatus.loading());
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
