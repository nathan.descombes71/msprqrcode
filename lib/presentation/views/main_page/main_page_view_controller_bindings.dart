import 'package:get/get.dart';

import '../views_mobile.exports.dart';
import 'main_page_view_controller.dart';

class MainPageViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => MainPageViewController(),
    );
  }
}
