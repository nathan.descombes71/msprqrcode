
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:msprqrcode/presentation/navigation/routes.dart';

import '../views_mobile.exports.dart';
import 'main_page_view_controller.dart';

class MainPageView extends GetView<MainPageViewController> {
  MainPageView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => buildContent(context));
  }

  Widget buildContent(BuildContext context){
   return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                  icon: Icon(MdiIcons.account),
                  onPressed: () {
                    // Get.toNamed(Routes.PROFILE);
                  })
            ],
            title: Text(
              "GoStyle",
            
            ),
          ),
          body: Center(child: Text('List'),),
        );
  }
}
