import 'package:flutter/material.dart';
import 'package:get/get.dart';


class MainPageViewController extends GetxController with StateMixin {
  MainPageViewController();

  PageController pageController = PageController();

  final RxInt currentIndex = 0.obs;

  @override
  void onInit() {
    change(null, status: RxStatus.loading());
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void changeTab(int index) {
    currentIndex.value = index;
    update();
  }

  void animateTo(int input) {
    pageController.animateToPage(
      input,
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }
}
