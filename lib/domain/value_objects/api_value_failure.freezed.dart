// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'api_value_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ApiValueFailureTearOff {
  const _$ApiValueFailureTearOff();

// ignore: unused_element
  NotFound<T> notFound<T>({@required String message}) {
    return NotFound<T>(
      message: message,
    );
  }

// ignore: unused_element
  JwtExpired<T> jwtExpired<T>({@required String message}) {
    return JwtExpired<T>(
      message: message,
    );
  }

// ignore: unused_element
  ServerError<T> serverError<T>({@required String message}) {
    return ServerError<T>(
      message: message,
    );
  }

// ignore: unused_element
  SerializationError<T> serializationError<T>({@required String message}) {
    return SerializationError<T>(
      message: message,
    );
  }

// ignore: unused_element
  Empty<T> empty<T>({@required String message}) {
    return Empty<T>(
      message: message,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ApiValueFailure = _$ApiValueFailureTearOff();

/// @nodoc
mixin _$ApiValueFailure<T> {
  String get message;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult notFound(String message),
    @required TResult jwtExpired(String message),
    @required TResult serverError(String message),
    @required TResult serializationError(String message),
    @required TResult empty(String message),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult notFound(String message),
    TResult jwtExpired(String message),
    TResult serverError(String message),
    TResult serializationError(String message),
    TResult empty(String message),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult notFound(NotFound<T> value),
    @required TResult jwtExpired(JwtExpired<T> value),
    @required TResult serverError(ServerError<T> value),
    @required TResult serializationError(SerializationError<T> value),
    @required TResult empty(Empty<T> value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult notFound(NotFound<T> value),
    TResult jwtExpired(JwtExpired<T> value),
    TResult serverError(ServerError<T> value),
    TResult serializationError(SerializationError<T> value),
    TResult empty(Empty<T> value),
    @required TResult orElse(),
  });

  @JsonKey(ignore: true)
  $ApiValueFailureCopyWith<T, ApiValueFailure<T>> get copyWith;
}

/// @nodoc
abstract class $ApiValueFailureCopyWith<T, $Res> {
  factory $ApiValueFailureCopyWith(
          ApiValueFailure<T> value, $Res Function(ApiValueFailure<T>) then) =
      _$ApiValueFailureCopyWithImpl<T, $Res>;
  $Res call({String message});
}

/// @nodoc
class _$ApiValueFailureCopyWithImpl<T, $Res>
    implements $ApiValueFailureCopyWith<T, $Res> {
  _$ApiValueFailureCopyWithImpl(this._value, this._then);

  final ApiValueFailure<T> _value;
  // ignore: unused_field
  final $Res Function(ApiValueFailure<T>) _then;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(_value.copyWith(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
abstract class $NotFoundCopyWith<T, $Res>
    implements $ApiValueFailureCopyWith<T, $Res> {
  factory $NotFoundCopyWith(
          NotFound<T> value, $Res Function(NotFound<T>) then) =
      _$NotFoundCopyWithImpl<T, $Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class _$NotFoundCopyWithImpl<T, $Res>
    extends _$ApiValueFailureCopyWithImpl<T, $Res>
    implements $NotFoundCopyWith<T, $Res> {
  _$NotFoundCopyWithImpl(NotFound<T> _value, $Res Function(NotFound<T>) _then)
      : super(_value, (v) => _then(v as NotFound<T>));

  @override
  NotFound<T> get _value => super._value as NotFound<T>;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(NotFound<T>(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$NotFound<T> with DiagnosticableTreeMixin implements NotFound<T> {
  const _$NotFound({@required this.message}) : assert(message != null);

  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiValueFailure<$T>.notFound(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ApiValueFailure<$T>.notFound'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NotFound<T> &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $NotFoundCopyWith<T, NotFound<T>> get copyWith =>
      _$NotFoundCopyWithImpl<T, NotFound<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult notFound(String message),
    @required TResult jwtExpired(String message),
    @required TResult serverError(String message),
    @required TResult serializationError(String message),
    @required TResult empty(String message),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return notFound(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult notFound(String message),
    TResult jwtExpired(String message),
    TResult serverError(String message),
    TResult serializationError(String message),
    TResult empty(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notFound != null) {
      return notFound(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult notFound(NotFound<T> value),
    @required TResult jwtExpired(JwtExpired<T> value),
    @required TResult serverError(ServerError<T> value),
    @required TResult serializationError(SerializationError<T> value),
    @required TResult empty(Empty<T> value),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult notFound(NotFound<T> value),
    TResult jwtExpired(JwtExpired<T> value),
    TResult serverError(ServerError<T> value),
    TResult serializationError(SerializationError<T> value),
    TResult empty(Empty<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class NotFound<T> implements ApiValueFailure<T> {
  const factory NotFound({@required String message}) = _$NotFound<T>;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  $NotFoundCopyWith<T, NotFound<T>> get copyWith;
}

/// @nodoc
abstract class $JwtExpiredCopyWith<T, $Res>
    implements $ApiValueFailureCopyWith<T, $Res> {
  factory $JwtExpiredCopyWith(
          JwtExpired<T> value, $Res Function(JwtExpired<T>) then) =
      _$JwtExpiredCopyWithImpl<T, $Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class _$JwtExpiredCopyWithImpl<T, $Res>
    extends _$ApiValueFailureCopyWithImpl<T, $Res>
    implements $JwtExpiredCopyWith<T, $Res> {
  _$JwtExpiredCopyWithImpl(
      JwtExpired<T> _value, $Res Function(JwtExpired<T>) _then)
      : super(_value, (v) => _then(v as JwtExpired<T>));

  @override
  JwtExpired<T> get _value => super._value as JwtExpired<T>;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(JwtExpired<T>(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$JwtExpired<T> with DiagnosticableTreeMixin implements JwtExpired<T> {
  const _$JwtExpired({@required this.message}) : assert(message != null);

  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiValueFailure<$T>.jwtExpired(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ApiValueFailure<$T>.jwtExpired'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is JwtExpired<T> &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $JwtExpiredCopyWith<T, JwtExpired<T>> get copyWith =>
      _$JwtExpiredCopyWithImpl<T, JwtExpired<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult notFound(String message),
    @required TResult jwtExpired(String message),
    @required TResult serverError(String message),
    @required TResult serializationError(String message),
    @required TResult empty(String message),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return jwtExpired(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult notFound(String message),
    TResult jwtExpired(String message),
    TResult serverError(String message),
    TResult serializationError(String message),
    TResult empty(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (jwtExpired != null) {
      return jwtExpired(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult notFound(NotFound<T> value),
    @required TResult jwtExpired(JwtExpired<T> value),
    @required TResult serverError(ServerError<T> value),
    @required TResult serializationError(SerializationError<T> value),
    @required TResult empty(Empty<T> value),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return jwtExpired(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult notFound(NotFound<T> value),
    TResult jwtExpired(JwtExpired<T> value),
    TResult serverError(ServerError<T> value),
    TResult serializationError(SerializationError<T> value),
    TResult empty(Empty<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (jwtExpired != null) {
      return jwtExpired(this);
    }
    return orElse();
  }
}

abstract class JwtExpired<T> implements ApiValueFailure<T> {
  const factory JwtExpired({@required String message}) = _$JwtExpired<T>;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  $JwtExpiredCopyWith<T, JwtExpired<T>> get copyWith;
}

/// @nodoc
abstract class $ServerErrorCopyWith<T, $Res>
    implements $ApiValueFailureCopyWith<T, $Res> {
  factory $ServerErrorCopyWith(
          ServerError<T> value, $Res Function(ServerError<T>) then) =
      _$ServerErrorCopyWithImpl<T, $Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class _$ServerErrorCopyWithImpl<T, $Res>
    extends _$ApiValueFailureCopyWithImpl<T, $Res>
    implements $ServerErrorCopyWith<T, $Res> {
  _$ServerErrorCopyWithImpl(
      ServerError<T> _value, $Res Function(ServerError<T>) _then)
      : super(_value, (v) => _then(v as ServerError<T>));

  @override
  ServerError<T> get _value => super._value as ServerError<T>;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(ServerError<T>(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$ServerError<T> with DiagnosticableTreeMixin implements ServerError<T> {
  const _$ServerError({@required this.message}) : assert(message != null);

  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiValueFailure<$T>.serverError(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ApiValueFailure<$T>.serverError'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ServerError<T> &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $ServerErrorCopyWith<T, ServerError<T>> get copyWith =>
      _$ServerErrorCopyWithImpl<T, ServerError<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult notFound(String message),
    @required TResult jwtExpired(String message),
    @required TResult serverError(String message),
    @required TResult serializationError(String message),
    @required TResult empty(String message),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return serverError(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult notFound(String message),
    TResult jwtExpired(String message),
    TResult serverError(String message),
    TResult serializationError(String message),
    TResult empty(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (serverError != null) {
      return serverError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult notFound(NotFound<T> value),
    @required TResult jwtExpired(JwtExpired<T> value),
    @required TResult serverError(ServerError<T> value),
    @required TResult serializationError(SerializationError<T> value),
    @required TResult empty(Empty<T> value),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult notFound(NotFound<T> value),
    TResult jwtExpired(JwtExpired<T> value),
    TResult serverError(ServerError<T> value),
    TResult serializationError(SerializationError<T> value),
    TResult empty(Empty<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class ServerError<T> implements ApiValueFailure<T> {
  const factory ServerError({@required String message}) = _$ServerError<T>;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  $ServerErrorCopyWith<T, ServerError<T>> get copyWith;
}

/// @nodoc
abstract class $SerializationErrorCopyWith<T, $Res>
    implements $ApiValueFailureCopyWith<T, $Res> {
  factory $SerializationErrorCopyWith(SerializationError<T> value,
          $Res Function(SerializationError<T>) then) =
      _$SerializationErrorCopyWithImpl<T, $Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class _$SerializationErrorCopyWithImpl<T, $Res>
    extends _$ApiValueFailureCopyWithImpl<T, $Res>
    implements $SerializationErrorCopyWith<T, $Res> {
  _$SerializationErrorCopyWithImpl(
      SerializationError<T> _value, $Res Function(SerializationError<T>) _then)
      : super(_value, (v) => _then(v as SerializationError<T>));

  @override
  SerializationError<T> get _value => super._value as SerializationError<T>;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(SerializationError<T>(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$SerializationError<T>
    with DiagnosticableTreeMixin
    implements SerializationError<T> {
  const _$SerializationError({@required this.message})
      : assert(message != null);

  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiValueFailure<$T>.serializationError(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
          DiagnosticsProperty('type', 'ApiValueFailure<$T>.serializationError'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SerializationError<T> &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $SerializationErrorCopyWith<T, SerializationError<T>> get copyWith =>
      _$SerializationErrorCopyWithImpl<T, SerializationError<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult notFound(String message),
    @required TResult jwtExpired(String message),
    @required TResult serverError(String message),
    @required TResult serializationError(String message),
    @required TResult empty(String message),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return serializationError(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult notFound(String message),
    TResult jwtExpired(String message),
    TResult serverError(String message),
    TResult serializationError(String message),
    TResult empty(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (serializationError != null) {
      return serializationError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult notFound(NotFound<T> value),
    @required TResult jwtExpired(JwtExpired<T> value),
    @required TResult serverError(ServerError<T> value),
    @required TResult serializationError(SerializationError<T> value),
    @required TResult empty(Empty<T> value),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return serializationError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult notFound(NotFound<T> value),
    TResult jwtExpired(JwtExpired<T> value),
    TResult serverError(ServerError<T> value),
    TResult serializationError(SerializationError<T> value),
    TResult empty(Empty<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (serializationError != null) {
      return serializationError(this);
    }
    return orElse();
  }
}

abstract class SerializationError<T> implements ApiValueFailure<T> {
  const factory SerializationError({@required String message}) =
      _$SerializationError<T>;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  $SerializationErrorCopyWith<T, SerializationError<T>> get copyWith;
}

/// @nodoc
abstract class $EmptyCopyWith<T, $Res>
    implements $ApiValueFailureCopyWith<T, $Res> {
  factory $EmptyCopyWith(Empty<T> value, $Res Function(Empty<T>) then) =
      _$EmptyCopyWithImpl<T, $Res>;
  @override
  $Res call({String message});
}

/// @nodoc
class _$EmptyCopyWithImpl<T, $Res>
    extends _$ApiValueFailureCopyWithImpl<T, $Res>
    implements $EmptyCopyWith<T, $Res> {
  _$EmptyCopyWithImpl(Empty<T> _value, $Res Function(Empty<T>) _then)
      : super(_value, (v) => _then(v as Empty<T>));

  @override
  Empty<T> get _value => super._value as Empty<T>;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(Empty<T>(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$Empty<T> with DiagnosticableTreeMixin implements Empty<T> {
  const _$Empty({@required this.message}) : assert(message != null);

  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiValueFailure<$T>.empty(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ApiValueFailure<$T>.empty'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Empty<T> &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  $EmptyCopyWith<T, Empty<T>> get copyWith =>
      _$EmptyCopyWithImpl<T, Empty<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult notFound(String message),
    @required TResult jwtExpired(String message),
    @required TResult serverError(String message),
    @required TResult serializationError(String message),
    @required TResult empty(String message),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return empty(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult notFound(String message),
    TResult jwtExpired(String message),
    TResult serverError(String message),
    TResult serializationError(String message),
    TResult empty(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (empty != null) {
      return empty(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult notFound(NotFound<T> value),
    @required TResult jwtExpired(JwtExpired<T> value),
    @required TResult serverError(ServerError<T> value),
    @required TResult serializationError(SerializationError<T> value),
    @required TResult empty(Empty<T> value),
  }) {
    assert(notFound != null);
    assert(jwtExpired != null);
    assert(serverError != null);
    assert(serializationError != null);
    assert(empty != null);
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult notFound(NotFound<T> value),
    TResult jwtExpired(JwtExpired<T> value),
    TResult serverError(ServerError<T> value),
    TResult serializationError(SerializationError<T> value),
    TResult empty(Empty<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class Empty<T> implements ApiValueFailure<T> {
  const factory Empty({@required String message}) = _$Empty<T>;

  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  $EmptyCopyWith<T, Empty<T>> get copyWith;
}
